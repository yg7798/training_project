package com.example.gamecricket.utility;

import lombok.Data;

@Data
public  class Constants {
    public final String HEAD="HEAD";
    public final String TAIL="TAIL";
}
