package com.example.gamecricket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


public class GameCricketApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameCricketApplication.class, args);
    }

}
